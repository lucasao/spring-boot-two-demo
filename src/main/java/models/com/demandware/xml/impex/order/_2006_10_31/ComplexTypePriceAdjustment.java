//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.07.19 at 12:54:17 PM CEST 
//


package com.demandware.xml.impex.order._2006_10_31;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for complexType.PriceAdjustment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="complexType.PriceAdjustment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.demandware.com/xml/impex/order/2006-10-31}complexType.LineItemAmounts"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="promotion-id" type="{http://www.demandware.com/xml/impex/order/2006-10-31}simpleType.Generic.NonEmptyString.256"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="campaign-id" type="{http://www.demandware.com/xml/impex/order/2006-10-31}simpleType.Generic.NonEmptyString.256" minOccurs="0"/&gt;
 *           &lt;element name="abtest" type="{http://www.demandware.com/xml/impex/order/2006-10-31}complexType.ABTestSegment" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="coupon-id" type="{http://www.demandware.com/xml/impex/order/2006-10-31}simpleType.Generic.NonEmptyString.256" minOccurs="0"/&gt;
 *         &lt;element name="reason-code" type="{http://www.demandware.com/xml/impex/order/2006-10-31}simpleType.Generic.NonEmptyString.256" minOccurs="0"/&gt;
 *         &lt;element name="manual" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="discount" type="{http://www.demandware.com/xml/impex/order/2006-10-31}complexType.AppliedDiscount" minOccurs="0"/&gt;
 *         &lt;element name="created-by" type="{http://www.demandware.com/xml/impex/order/2006-10-31}simpleType.Generic.String.256" minOccurs="0"/&gt;
 *         &lt;element name="creation-date" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="custom-attributes" type="{http://www.demandware.com/xml/impex/order/2006-10-31}sharedType.CustomAttributes" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "complexType.PriceAdjustment", propOrder = {
    "promotionId",
    "campaignId",
    "abtest",
    "couponId",
    "reasonCode",
    "manual",
    "discount",
    "createdBy",
    "creationDate",
    "customAttributes"
})
public class ComplexTypePriceAdjustment
    extends ComplexTypeLineItemAmounts
{

    @XmlElement(name = "promotion-id", required = true)
    protected String promotionId;
    @XmlElement(name = "campaign-id")
    protected String campaignId;
    protected ComplexTypeABTestSegment abtest;
    @XmlElement(name = "coupon-id")
    protected String couponId;
    @XmlElement(name = "reason-code")
    protected String reasonCode;
    protected Boolean manual;
    protected ComplexTypeAppliedDiscount discount;
    @XmlElement(name = "created-by")
    protected String createdBy;
    @XmlElement(name = "creation-date")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDate;
    @XmlElement(name = "custom-attributes")
    protected SharedTypeCustomAttributes customAttributes;

    /**
     * Gets the value of the promotionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionId() {
        return promotionId;
    }

    /**
     * Sets the value of the promotionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionId(String value) {
        this.promotionId = value;
    }

    /**
     * Gets the value of the campaignId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Sets the value of the campaignId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampaignId(String value) {
        this.campaignId = value;
    }

    /**
     * Gets the value of the abtest property.
     * 
     * @return
     *     possible object is
     *     {@link ComplexTypeABTestSegment }
     *     
     */
    public ComplexTypeABTestSegment getAbtest() {
        return abtest;
    }

    /**
     * Sets the value of the abtest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexTypeABTestSegment }
     *     
     */
    public void setAbtest(ComplexTypeABTestSegment value) {
        this.abtest = value;
    }

    /**
     * Gets the value of the couponId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponId() {
        return couponId;
    }

    /**
     * Sets the value of the couponId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponId(String value) {
        this.couponId = value;
    }

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the manual property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isManual() {
        return manual;
    }

    /**
     * Sets the value of the manual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setManual(Boolean value) {
        this.manual = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link ComplexTypeAppliedDiscount }
     *     
     */
    public ComplexTypeAppliedDiscount getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexTypeAppliedDiscount }
     *     
     */
    public void setDiscount(ComplexTypeAppliedDiscount value) {
        this.discount = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link SharedTypeCustomAttributes }
     *     
     */
    public SharedTypeCustomAttributes getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedTypeCustomAttributes }
     *     
     */
    public void setCustomAttributes(SharedTypeCustomAttributes value) {
        this.customAttributes = value;
    }

}
