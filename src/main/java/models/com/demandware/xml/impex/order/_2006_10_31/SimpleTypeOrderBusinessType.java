//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.07.19 at 12:54:17 PM CEST 
//


package com.demandware.xml.impex.order._2006_10_31;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for simpleType.OrderBusinessType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="simpleType.OrderBusinessType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="B2C"/&gt;
 *     &lt;enumeration value="B2B"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "simpleType.OrderBusinessType")
@XmlEnum
public enum SimpleTypeOrderBusinessType {

    @XmlEnumValue("B2C")
    B_2_C("B2C"),
    @XmlEnumValue("B2B")
    B_2_B("B2B");
    private final String value;

    SimpleTypeOrderBusinessType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SimpleTypeOrderBusinessType fromValue(String v) {
        for (SimpleTypeOrderBusinessType c: SimpleTypeOrderBusinessType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
