//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.07.19 at 12:54:17 PM CEST 
//


package com.demandware.xml.impex.order._2006_10_31;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for complexType.ProductShippingLineItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="complexType.ProductShippingLineItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.demandware.com/xml/impex/order/2006-10-31}complexType.LineItemAmounts"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="quantity" type="{http://www.demandware.com/xml/impex/order/2006-10-31}complexType.Quantity"/&gt;
 *         &lt;element name="tax-rate" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="type" type="{http://www.demandware.com/xml/impex/order/2006-10-31}simpleType.ProductShippingCostType"/&gt;
 *         &lt;element name="price-adjustments" type="{http://www.demandware.com/xml/impex/order/2006-10-31}complexType.PriceAdjustments" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "complexType.ProductShippingLineItem", propOrder = {
    "quantity",
    "taxRate",
    "type",
    "priceAdjustments"
})
public class ComplexTypeProductShippingLineItem
    extends ComplexTypeLineItemAmounts
{

    @XmlElement(required = true)
    protected ComplexTypeQuantity quantity;
    @XmlElement(name = "tax-rate")
    protected double taxRate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected SimpleTypeProductShippingCostType type;
    @XmlElement(name = "price-adjustments")
    protected ComplexTypePriceAdjustments priceAdjustments;

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link ComplexTypeQuantity }
     *     
     */
    public ComplexTypeQuantity getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexTypeQuantity }
     *     
     */
    public void setQuantity(ComplexTypeQuantity value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the taxRate property.
     * 
     */
    public double getTaxRate() {
        return taxRate;
    }

    /**
     * Sets the value of the taxRate property.
     * 
     */
    public void setTaxRate(double value) {
        this.taxRate = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleTypeProductShippingCostType }
     *     
     */
    public SimpleTypeProductShippingCostType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleTypeProductShippingCostType }
     *     
     */
    public void setType(SimpleTypeProductShippingCostType value) {
        this.type = value;
    }

    /**
     * Gets the value of the priceAdjustments property.
     * 
     * @return
     *     possible object is
     *     {@link ComplexTypePriceAdjustments }
     *     
     */
    public ComplexTypePriceAdjustments getPriceAdjustments() {
        return priceAdjustments;
    }

    /**
     * Sets the value of the priceAdjustments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexTypePriceAdjustments }
     *     
     */
    public void setPriceAdjustments(ComplexTypePriceAdjustments value) {
        this.priceAdjustments = value;
    }

}
