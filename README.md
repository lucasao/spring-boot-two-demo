# Demo spring boot app
this demo app was initialized with Spring Initializr (https://start.spring.io/)

## Build
```bash
mvn package -e
```

## Run
```bash
java -jar target/demo-0.0.1-SNAPSHOT.jar
```

